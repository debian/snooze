snooze (0.5-1) unstable; urgency=medium

  * New upstream release (2021-01-14)
    + Add jitter with the `-J` option
    + 2 minor bugfixes

  * Upgrade to debhelper compatibility level 13
    `dh_missing --fail-missing` is now the default.
  * d/control: Declare compliance with policy v4.5.1.
    No change needed.

 -- nicoo <nicoo@debian.org>  Thu, 14 Jan 2021 16:48:47 +0100

snooze (0.4-3) unstable; urgency=medium

  [ Helmut Grohne ]
  * Fix FTCBFS: Let dh_auto_build pass cross tools to make (Closes: #951529)

  [ nicoo ]
  * Salsa CI: Remove the configuration file
    Instead, we use 'salsa-ci.yml@nicoo/conventions' as the config path.

  * d/changelog: Update nicoo's name and address
  * d/control: Update upstream's Homepage
  * Use upstream's canonical repository at vuxu.org in d/{copyright, watch}

 -- nicoo <nicoo@debian.org>  Mon, 02 Nov 2020 11:31:48 +0100

snooze (0.4-2) unstable; urgency=medium

  * debian/rules
    + Honor build flags (incl. hardening)
    + Fail build on dh_missing
  * Setup CI using salsa-ci-team's pipeline

 -- nicoo <nicoo@debian.org>  Sat, 15 Feb 2020 23:52:28 +0100

snooze (0.4-1) unstable; urgency=medium

  * New upstream release (2020-02-07)

  * debian/control: Comply with policy v4.5.0
    + Set Rules-Requires-Root: no
  * Upgrade to debhelper compatibility level 12.
    Replace the legacy debian/compat with Build-Depends: debhelper-compat (= 12)

  * debian/watch: Update to uscan v4, check signatures on git tags
    + Remove obsoleted Lintian overrides

  * Move the packaging branch to debian/sid
  * Remove superfluous use of dh-exec
  * Update authorship information
  * Update upstream's signing key
  * Upstream repository relocated to https://github.com/leahneukirchen

 -- nicoo <nicoo@debian.org>  Wed, 12 Feb 2020 18:45:14 +0100

snooze (0.3-1) unstable; urgency=medium

  * New upstream version (2018-02-13)
  * Move packaging repository to salsa.d.o
  * Declare compliance with policy v4.1.4.
    No change needed.
  * Remove superfluous use of dh-exec

 -- nicoo <nicoo@debian.org>  Wed, 09 May 2018 22:04:59 +0200

snooze (0.2-2) unstable; urgency=medium

  * debian/control: Fixup description
  * debian/rules
    * Do not spuriously disable tests
    * Remove spurious --parallel

  * Ship upstream's changelog
  * Switch to debhelper 11

  * Lintian overrides
    * Relocate to debian/source/
    * Rename debian-watch-may-check-gpg-signature
    * Add debian-watch-could-verify-download

  * Bump Standards-Version to 4.1.3
    * debian/copyright: Refer to the CC0 license file
    * debian/copyright: Use HTTPS URI for DEP5 format

 -- nicoo <nicoo@debian.org>  Tue, 13 Feb 2018 21:32:42 +0100

snooze (0.2-1) unstable; urgency=medium

  * Initial packaging (Closes: 882814)

 -- nicoo <nicoo@debian.org>  Mon, 27 Nov 2017 01:44:37 +0100
